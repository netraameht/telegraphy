"use strict"

import { ipcMain, app } from 'electron';
import DataStore from 'nedb';
import { ipc } from './Constants';
import path from 'path';
let self;

export class AccountManager {
  constructor() {
    self = this;
    self.accountsFile = path.join(app.getPath('userData'), 'accounts.db');
    self.loaded = false;
    self.store = new DataStore({filename: self.accountsFile, autoload: true, onload: () => self.loaded = true})

    ipcMain.on(ipc.ACCOUNTS_LIST_REQUEST, (event) => {
      self.store.find({hidden: false}, (err, docs) => {
        if(err) throw err;
        event.sender.send(ipc.ACCOUNTS_LIST_FULL, [{
          hidden: false,
          name: 'Someone',
          email: 'Some@Email'
        }]);
      });
    });
  }
}

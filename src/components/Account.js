"use strict";

import React from 'react';
import { ipcRenderer } from 'electron';
import { ipc } from '../libs/Constants';
let self;

export class Account extends React.Component {
  constructor(props) {
    super(props);
    self = this;
  }
  render() {
    console.log('Rendering ', self.props.account)
    let classes = 'account'
    if(self.props.active) classes += ' active'
    return (
      <div className={classes}></div>
    );
  }
}

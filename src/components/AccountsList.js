"use strict";

import React from 'react';
import { ipcRenderer } from 'electron';
import { ipc } from '../libs/Constants';
import { Account } from './Account'
let self;

export class AccountsList extends React.Component {
  constructor(props) {
    super(props);
    self = this;
    self.state = {accounts: []};
    ipcRenderer.send(ipc.ACCOUNTS_LIST_REQUEST)
    ipcRenderer.on(ipc.ACCOUNTS_LIST_FULL, (event, accounts) => {
      self.setState({accounts})
    });
  }
  render() {
    return (
      <nav className="accounts">
        {self.state.accounts.forEach(account =>
          <Account account={account} active={true} />
        )}
      </nav>
    );
  }
}
